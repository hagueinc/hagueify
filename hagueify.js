/****************************************
 *             DOCUMENTATION            *
 * hagueify() - replace most text with  *
 *   "jacob hague"                      *
 * haguebomb() - replace even more text *
 *   with "jacob hague"                 *
 * hagueimage() - replace all images    *
 *   with a random Jacob Hague          *
 * haguelink() - replace all links to   *
 *   go to hagueinc.ml                  *
 * hagueads() - replace detectable ads  *
 *   (usually google ads) with Hague    *
 *   product ads                        *
 * HAGUE() - runs the recommended combo *
 *                                      *
 *         GOOD WEBSITES TO TRY         *
 *       scroll down to the bottom      *
 *                                      *
 *              ANALYTICS               *
 * https://hagueinc.goatcounter.com/    *
 * No personal information stored       *
 *                                      *
 * Remove consent by changing the below *
 * value to false                       *
 ****************************************/
 
var ANALYTICS_CONSENT = true;

Array.prototype.random = function () {
  return this[Math.floor((Math.random()*this.length))];
};

function _hague_replacetext(regex){
    var all = document.getElementsByTagName("*");
    for(var i=0,max=all.length;i<max;i++){
        try {
            if(!all[i].innerHTML.replace(regex,"")
                                .includes("<")){
                all[i].innerHTML="jacob hague";
            }
        } catch (e) {
            console.log(e.message);
        }
    }
}

function hagueify(){
    _hague_track("hagueify");
    var regex=/<(\/)*br([^/>]*)?(\/)*>/gi;
    _hague_replacetext(regex);
}

function haguebomb(){
    _hague_track("haguebomb")
    var regex=/<(\/)*((br)|(sup)|a|(span)|(small))([^/>]*)?(\/)*>/gi;
    _hague_replacetext(regex);
}

function hagueimage(){
    // hosted by JD
    _hague_track("hagueimage")
    var images=[
        "https://i.ibb.co/6Y0m5RS/Jacob-Head-2-1.png",
        "https://i.ibb.co/GThgyC7/jacob-head.png",
        "https://i.ibb.co/KxN7NT3/jacob-head-sunglasses.png",
        "https://i.ibb.co/pXfNFZX/jacob-head-4.png",
        "https://i.ibb.co/c6N0td0/jacob-head-3.png",
        "https://i.ibb.co/sydnvfQ/jacob-head-2.png"
    ];
    var imgTags = document.getElementsByTagName('img'); 
    for(var i=0,max=imgTags.length;i<max;i++){
        try {
            var img = images.random();
            imgTags[i].src = img;
            imgTags[i].srcset = img; // for wikipedia
        } catch (e) {
            console.log(e.message);
        }
    }
}

function haguelink(){
    _hague_track("haguelink")
    var aTags = document.getElementsByTagName('a'); 
    for(var i=0,max=aTags.length;i<max;i++){
        try {
            aTags[i].href = "http://hagueinc.ml";
        } catch (e) {
            console.log(e.message);
        }
    }
}

function hagueads(){
    // hosted by JD
    _hague_track("hagueads")
    var ads = [
            "https://i.ibb.co/8xv7G0p/Hagueulator-2.jpg",
            "https://i.ibb.co/4YX3Ntf/Halexa.jpg",
            "https://i.ibb.co/vYJL0hN/Measurements.jpg",
            "https://i.ibb.co/swrbvH2/Hague-Life-Insurance.jpg",
            "https://i.ibb.co/PjqXGcG/Hagueies.jpg"
        ];
    var divTags = document.getElementsByTagName('div'); 
    for(var i=0,max=divTags.length;i<max;i++){
        try {
            var tag = divTags[i];
            if(tag.id.includes("ads")){
                var img = document.createElement("img");
                img.src = ads.random();
                img.width = tag.clientWidth;
                img.height = tag.clientHeight;
                tag.innerHTML = "";
                tag.appendChild(img);
            }
        } catch (e) {
            console.log(e.message);
        }
    }
}

function HAGUE(){
    console.log("Text...")
    hagueify();
    console.log("Images...")
    hagueimage();
    console.log("Ads...")
    hagueads();
    console.log("DONE")
}


function _hague_track(thing){
    if(!ANALYTICS_CONSENT) return;
    var pixel="https://hagueinc.goatcounter.com/count?p=/"+thing;
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", pixel, true);
    xhttp.send();
}

_hague_track("load")


/*******************************************************************************
 * WEBSITES TO TRY
 * 
 * Google Images [use hagueimage()]
 * speedtest.net
 * The Apple iPhone website
 * The Google Store
 * amazon.co.uk [breaks, but still funny]
 * Google Drive [use hagueimage()]
 * Ecosia.org search results
 * 
 * WEBSITES THAT BREAK BIG TIME USING HAGUE() OR hagueify() OR haguebomb()
 * Twitter
 * Google Images
 * Google Drive
 * Google
*******************************************************************************/